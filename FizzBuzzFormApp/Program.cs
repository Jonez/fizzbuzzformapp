using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FizzBuzzFormApp
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        static int i;

        public static string StartFizzBuzz()
        {
            string FizzBuzzText = "";
            string newLine = Environment.NewLine;
            for (i = 1; i <= 100; i++)
                {
                    if (i % 3 == 0 && i % 5 == 0) FizzBuzzText += "FizzBuzz" + newLine;
                    else if (i % 3 == 0) FizzBuzzText += "Fizz" + newLine;
                    else if (i % 5 == 0) FizzBuzzText += "Buzz" + newLine;
                    else FizzBuzzText += i.ToString() + newLine;

                }
                return FizzBuzzText;
        }

        public static int ProgressBarNum()
        {
            return i - 1;
        }
    }
}
